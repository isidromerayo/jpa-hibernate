Learning JPA 2 with Hibernate
=============================

I try to learn use JPA with Hibernate

What I want to learn
--------------------

* Envers

Use
---

$ mvn clean verify

References
----------

[Hibernate Envers: Getting started](http://www.thoughts-on-java.org/hibernate-envers-getting-started/)
[Hibernate Envers: Query data from your audit log](http://www.thoughts-on-java.org/hibernate-envers-query-data-audit-log/)
[Hibernate Envers: Extend the standard revision](http://www.thoughts-on-java.org/hibernate-envers-extend-standard-revision/)