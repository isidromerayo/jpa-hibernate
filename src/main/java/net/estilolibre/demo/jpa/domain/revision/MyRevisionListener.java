/**
 * 
 */
package net.estilolibre.demo.jpa.domain.revision;

import java.util.Random;

import org.hibernate.envers.RevisionListener;

/**
 * @author isidromerayo
 *
 */
public class MyRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		MyRevision rev = (MyRevision) revisionEntity;
		rev.setUserName(getUserName());
		
	}

	private String getUserName() {
		Random rnd = new Random();
		double value = rnd.nextInt();
		if (value >= 0.2) {
			return "User 1";
		}
		return "User " + value;
		
	}

}
