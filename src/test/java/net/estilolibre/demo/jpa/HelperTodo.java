/**
 * 
 */
package net.estilolibre.demo.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;

import net.estilolibre.demo.jpa.domain.Todo;

/**
 * @author isidromerayo
 *
 */
public class HelperTodo {

	public static Todo generateVersionTodo(EntityManager em) {
		Todo todo = new Todo();
		todo.setTitle("Hibernate Tips – 64 Tips for your day to day work");
		exampleSave(todo, em);
		exampleSave(todo, em);
		exampleSave(todo, em);
		return todo;
	}

	private static void exampleSave(Todo todo, EntityManager em) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		em.getTransaction().begin();
		if (todo.getId()==null) {
			em.persist(todo);
		}
		else {
			todo.setDescription("Modify title on " + dateFormat.format(new Date()));
			em.merge(todo);
		}

		em.getTransaction().commit();
		
	}

}
