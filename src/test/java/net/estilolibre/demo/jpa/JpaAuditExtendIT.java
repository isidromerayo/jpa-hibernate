/**
 * 
 */
package net.estilolibre.demo.jpa;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.estilolibre.demo.jpa.domain.Todo;

/**
 * @author isidromerayo
 *
 */
public class JpaAuditExtendIT {

	private static final Logger logger = LoggerFactory.getLogger(JpaAuditExtendIT.class);
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-hibernate_PU") ;
	private static EntityManager manager;
	
	@Before
	public void setUp() {
		manager = factory.createEntityManager();
	}
	
	@After
	public void tearDown() {
		manager.close();
	}
	@Test
	public final void revisionDataExtendByUserName() {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		
		Todo todo = HelperTodo.generateVersionTodo(manager);
		
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, false, true);
		q.addProjection(AuditEntity.revisionNumber());
		q.add(AuditEntity.revisionProperty("userName").eq("User 1"));
		List<Number> revisionNumbers = q.getResultList();
		assertThat(revisionNumbers.size(), greaterThanOrEqualTo(1));
		for (Number revisionTodo : revisionNumbers) {
			Todo auditedTodo = auditReader.find(Todo.class, todo.getId(), revisionTodo);
			logger.info("Number: " + revisionTodo.intValue() + " " + auditedTodo);
		}
	}

}
