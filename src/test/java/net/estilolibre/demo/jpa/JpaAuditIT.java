/**
 * 
 */
package net.estilolibre.demo.jpa;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import org.hibernate.criterion.MatchMode;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.estilolibre.demo.jpa.domain.Todo;

/**
 * @author isidromerayo
 *
 */
public class JpaAuditIT {
	private static final Logger logger = LoggerFactory.getLogger(JpaAuditIT.class);
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-hibernate_PU") ;
	private static EntityManager manager;
	
	@Before
	public void setUp() {
		manager = factory.createEntityManager();
	}
	
	@After
	public void tearDown() {
		manager.close();
	}
	@Test
	public final void shouldBeAuditEntityWithEnvers() {
		
		Todo todo = generateVersionTodo();
		
		AuditReader auditReader = AuditReaderFactory.get(manager);
		List<Number> revisionNumbers = auditReader.getRevisions(Todo.class, todo.getId());
		assertThat(revisionNumbers.size(), greaterThanOrEqualTo(2));
		for (Number revisionTodo : revisionNumbers) {
			Todo auditedTodo = auditReader.find(Todo.class, todo.getId(), revisionTodo);
			logger.info("["+auditedTodo+"] at revision ["+revisionTodo+"].");
		}

	}

	private Todo generateVersionTodo() {
		Todo todo = new Todo();
		todo.setTitle("Hibernate Tips – 64 Tips for your day to day work");
		exampleSave(todo);
		exampleSave(todo);
		exampleSave(todo);
		return todo;
	}

	private void exampleSave(Todo todo) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		manager.getTransaction().begin();
		if (todo.getId()==null) {
			manager.persist(todo);
		}
		else {
			todo.setDescription("Modify title on " + dateFormat.format(new Date()));
			manager.merge(todo);
		}

		manager.getTransaction().commit();
	}

	
	private void exampleSave3(Todo todo) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		todo.setTitle("Candidate to delete #3");
		todo.setDescription("Modify title on " + dateFormat.format(new Date()));
		manager.getTransaction().begin();
		manager.persist(todo);
		manager.getTransaction().commit();
		
	}

	
	private void exampleDelete(Todo todo) {
		manager.getTransaction().begin();
		manager.remove(todo);
		manager.getTransaction().commit();
	}
	
	@Test
	public final void whenVersionNumberNotExistsIsTheSameAtLastVersion() {
		Todo todo = generateVersionTodo();
		AuditReader auditReader = AuditReaderFactory.get(manager);
		
		Todo auditedTodoV2 = auditReader.find(Todo.class, todo.getId(), 2);
		Todo auditedTodoV3 = auditReader.find(Todo.class, todo.getId(), 3);
		
		assertEquals(auditedTodoV2, auditedTodoV3);
	}
	@Test
	public final void forRevisionsOfEntityVerticalQuery() {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		/* 
		 * Alternatives queries: 
		 * 		- selectedEntitiesOnly
		 * 		- selectDeletedEntities
		 */
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, true, true);
		Todo todo = generateVersionTodo();
		q.add(AuditEntity.id().eq(todo.getId()));
		List<Todo> audit = q.getResultList();
		
		assertThat(audit.size(), greaterThanOrEqualTo(2));

	}
	@Test
	public final void shouldBeReturnFirstVersionVerticalQuery() {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		Todo todo = generateVersionTodo();
		
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, false, true);
		q.addProjection(AuditEntity.revisionNumber().min());
		q.add(AuditEntity.id().eq(todo.getId()));
		q.add(AuditEntity.property("title").eq("Hibernate Tips – 64 Tips for your day to day work"));
		
		Number revision = (Number) q.getSingleResult();
		
		assertThat(revision.intValue(), greaterThan(1));

	}
	@Test
	public final void shouldBeReturnLastVersionVerticalQuery() {
		Todo todo = generateVersionTodo();
		AuditReader auditReader = AuditReaderFactory.get(manager);
		
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, false, true);
		q.addProjection(AuditEntity.revisionNumber().max());
		q.add(AuditEntity.id().eq(todo.getId()));
		q.add(AuditEntity.property("title").eq("Hibernate Tips – 64 Tips for your day to day work"));
		
		Number revision = (Number) q.getSingleResult();
		
		assertThat(revision.intValue(), greaterThanOrEqualTo(2));

	}

	@Test(expected=NoResultException.class)
	public final void deleteRevisionOfEntityVerticalQuery() {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, true, true);
		Todo todo = generateVersionTodo();
		exampleDelete(todo);
		q.add(AuditEntity.id().eq(todo.getId()));
		List<Todo> audit = q.getResultList();
		int i = 0;
		// Include delete revisions
		for (Todo todo2 : audit) {
			logger.info("Version #" + (++i) + " of entity with id "+ todo.getId() +" - " + todo2);
		}
		
		Todo deleteTodo = (Todo) manager.createQuery("SELECT t FROM Todo t WHERE t.id = :todo_id").setParameter("todo_id", todo.getId()).getSingleResult();

	}
	@Test(expected=NoResultException.class)
	public final void noDeleteRevisionOfEntityVerticalQuery() {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Todo.class, true, false);
		Todo todo = generateVersionTodo();
		exampleDelete(todo);
		q.add(AuditEntity.id().eq(todo.getId()));
		List<Todo> audit = q.getResultList();
		int i = 0;
		for (Todo todo2 : audit) {
			logger.info("Version #" + (++i) + " of entity with id "+ todo.getId() +" - " + todo2);
		}
		
		Todo deleteTodo = (Todo) manager.createQuery("SELECT t FROM Todo t WHERE t.id = :todo_id").setParameter("todo_id", todo.getId()).getSingleResult();

	}
	
	@Test
	public final void horizontalQuery() {
		AuditReader auditReader = AuditReaderFactory.get(manager);

		AuditQuery q = auditReader.createQuery().forEntitiesAtRevision(Todo.class, 2);
		q.add(AuditEntity.property("title").like("Hibernate", MatchMode.ANYWHERE));
		q.addOrder(AuditEntity.property("title").asc());
		List<Todo> audit = q.getResultList();
		assertThat(audit.size(), equalTo(1));
		
		Long idTodo = null;
		for (Todo todo : audit) {
			idTodo = todo.getId();
			logger.info("Find at revision in 2 position: " + todo);
		}
		
		List<Number> revisionNumbers = auditReader.getRevisions(Todo.class, idTodo);
		assertThat(revisionNumbers.size(), greaterThanOrEqualTo(2));
		for (Number revisionTodo : revisionNumbers) {
			Todo auditedTodo = auditReader.find(Todo.class, idTodo, revisionTodo);
			logger.info("Entity "+auditedTodo+" at revision #"+revisionTodo);
		}

	}


}
