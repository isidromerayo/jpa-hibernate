package net.estilolibre.demo.jpa;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.estilolibre.demo.jpa.domain.Todo;

public class JpaIT {

	private static final Logger logger = LoggerFactory.getLogger(JpaIT.class);
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-hibernate_PU") ;
	private static EntityManager manager;
	
	@Before
	public void setUp() {
		manager = factory.createEntityManager();
	}
	
	@After
	public void tearDown() {
		manager.close();
	}
	
	@Test
	public final void shouldBeInitialLoadFromPersistenceXML() {
		List<Todo> result = getAllTodo();
		assertThat(result.size(), greaterThanOrEqualTo(3));
	}

	@SuppressWarnings("unchecked")
	private List<Todo> getAllTodo() {
		return manager.createQuery("SELECT t FROM Todo t").getResultList();
	}

}
